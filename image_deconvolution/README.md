# Wiener filter
## Как запустить
TL;DR
```sh
git clone <repo>
cd <repo>
python -m virtualenv env
source env/bin/activate
pip install -r requirements.txt
```

## Sources
* [Деконволюция с каким то произвольным ядром](https://stackoverflow.com/questions/40713929/weiner-deconvolution-using-opencv)
* [Фильтр Винера](https://en.wikipedia.org/wiki/Wiener_filter)

## Пример работы
```sh
python main.py \
    --filename flower.jpeg \
    --k-from 0.5 \
    --k-to 0.0001 \
    --noise 0.05 \
    --kernel-size 15 \
    --frames 100 \
    --output video_2.mp4
```

Изображение будет преобразованно к размеру 512x512 с чёрной рамкой, размыто. К нему будет добавлен шум, и затем последовательно будет применен 100 раз алгоритм Винера, с разными значениями параметра K. Из полученных 100 кадров будет составлено видео.

### Исходное изображение
![Исходное изображение](flower.jpeg)

### Полученное видео
![Видео](video_2.mp4)