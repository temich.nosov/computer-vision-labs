numpy==1.19.5
opencv-python==4.5.1.48
scipy==1.5.4
sk-video==1.1.10
tqdm==4.58.0
