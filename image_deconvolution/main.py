import argparse
import cv2 as cv
import numpy as np
import math
import skvideo.io
from tqdm import tqdm

def get_kernel(size_x, size_y, size):
    zeroes = np.zeros((size_x, size_y), np.float64)
    zeroes[size // 2][size // 2] = 1.
    res = cv.GaussianBlur(zeroes, (size, size), 0)
    return res[:size_x,:size_y]

def complex_number_inverse(re_a, im_a):
    x = re_a * re_a + im_a * im_a
    return (re_a / x, -im_a / x)

def complex_number_mult(re_a, im_a, re_b, im_b):
    return (re_a * re_b - im_a * im_b, re_a * im_b + im_a * re_b)
    # return ((re_a * re_b + im_a * im_b) / x, (im_a * re_b - re_a * im_b) / x)

def complex_number_division(re_a, im_a, re_b, im_b):
    x = re_b * re_b + im_b * im_b
    return ((re_a * re_b + im_a * im_b) / x, (im_a * re_b - re_a * im_b) / x)

def perform_dft(img, kernel, k):
    rows, cols = img.shape
    optimal_rows = cv.getOptimalDFTSize(rows)
    optimal_cols = cv.getOptimalDFTSize(cols)

    padded = cv.copyMakeBorder(img, 0, optimal_rows - rows, 0, optimal_cols - cols, cv.BORDER_CONSTANT, value=[0, 0, 0])

    kernel = cv.copyMakeBorder(kernel, 0, optimal_rows - kernel.shape[0], 0, optimal_cols - kernel.shape[1], cv.BORDER_CONSTANT, value=[0, 0, 0])
    blured_kernel = cv.merge([
        kernel,
        np.zeros((optimal_rows, optimal_cols), np.float64)])
    cv.dft(blured_kernel, blured_kernel)

    planes = [np.float64(padded), np.zeros(padded.shape, np.float64)]
    complexI = cv.merge(planes)
    cv.dft(complexI, complexI)

    eps = 1e-9
    for x in range(optimal_rows):
        for y in range(optimal_cols):
            t = (blured_kernel[x][y][0], blured_kernel[x][y][1])
            mg = t[0] * t[0] + t[1] * t[1]
            A = mg / (mg + k)

            # t = (t[0] + eps, t[1] + eps)
            t = complex_number_inverse(t[0], t[1])
            z = complex_number_mult(complexI[x][y][0], complexI[x][y][1], t[0], t[1])
            complexI[x][y][0] = z[0] * A
            complexI[x][y][1] = z[1] * A

    cv.idft(complexI, complexI)
    cv.split(complexI, planes)
    image_magnitude = planes[0]
    cv.magnitude(planes[0], planes[1], image_magnitude)
    cv.normalize(image_magnitude, image_magnitude, 0, 1, cv.NORM_MINMAX)
    return image_magnitude

def to_image(np_arr):
    return np.uint8(np_arr * 255)

def main():
    parser = argparse.ArgumentParser(description='Perform wiener transformation')
    parser.add_argument('--filename', dest='filename', required=True)
    parser.add_argument('--noise', dest='noise', default=0.15, type=float, help='Noise level')
    parser.add_argument('--kernel-size', dest='kernel_size', default=31, type=int, help='Gauss kernel size')
    parser.add_argument('--image-size', dest='image_size', default=512, type=int, help='Output frame size')
    parser.add_argument('--k-from', dest='k_from', default=10., type=float, help='K from')
    parser.add_argument('--k-to', dest='k_to', default=0.000001, type=float, help='K to')
    parser.add_argument('--frames', dest='frames', default=300, type=int, help='K to')
    parser.add_argument('--output', dest='output', required=True, help='output file (should be mp4)')
    args = parser.parse_args()
    
    KERNEL_SIZE = args.kernel_size
    IMG_SIZE = args.image_size
    BS = args.noise
    source_image = cv.imread(cv.samples.findFile(args.filename), cv.IMREAD_GRAYSCALE)
    source_image = cv.resize(source_image, (IMG_SIZE - KERNEL_SIZE * 2, IMG_SIZE - KERNEL_SIZE * 2))
    source_image = cv.copyMakeBorder(source_image, KERNEL_SIZE, KERNEL_SIZE, KERNEL_SIZE, KERNEL_SIZE, cv.BORDER_CONSTANT, value=[0, 0, 0])
    source_image = np.float64(source_image) / 255

    blured_image = cv.GaussianBlur(source_image, (KERNEL_SIZE, KERNEL_SIZE), 0)
    blured_image = (blured_image + BS * np.random.uniform(0., 1., blured_image.shape)) / (1.5)
    kernel = get_kernel(IMG_SIZE, IMG_SIZE, KERNEL_SIZE)

    cv.normalize(source_image, source_image, 0, 1, cv.NORM_MINMAX)

    k_from = args.k_from
    k_to = args.k_to
    FRAMES = args.frames

    out_video = np.empty([FRAMES, IMG_SIZE, IMG_SIZE, 3], dtype = np.uint8)
    out_video = out_video.astype(np.uint8)

    for frame in tqdm(range(FRAMES)):
        k_from_log = math.log(k_from)
        k_to_log = math.log(k_to)
        k = math.exp(k_from_log + frame * (k_to_log - k_from_log) / (FRAMES - 1))

        rs = to_image(perform_dft(blured_image, kernel, k))
        cv.putText(rs, 'K=' + "{:.7f}".format(k), (10, IMG_SIZE - 10), cv.FONT_HERSHEY_COMPLEX, 1, (255, 255, 255), 2, cv.LINE_AA)
        out_video[frame] = cv.merge([rs, rs, rs])
    skvideo.io.vwrite(args.output, out_video, outputdict={"-pix_fmt": "yuv420p"})

if __name__ == "__main__":
    main()