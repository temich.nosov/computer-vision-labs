import argparse
import cv2 as cv
import numpy as np
import skvideo.io
from tqdm import tqdm
from skimage import metrics
import math

def compress_jpeg_q(img, q):
    enc = cv.imencode('.jpg', img, [cv.IMWRITE_JPEG_QUALITY, 100 * q])[1]
    return cv.imdecode(enc, 1).astype(img.dtype) 

def compute_mse(img1, img2):
    err = np.sum((img1.astype("float") - img2.astype("float")) ** 2)
    err /= float(img1.shape[0] * img1.shape[1] * img1.shape[2])
    return err

def compute_psnr(img1, img2):
    mse = compute_mse(img1, img2)
    return 20 * math.log10(255 / math.sqrt(mse))

def compute_ssim(img1, img2):
    return metrics.structural_similarity(img1, img2, multichannel=True)

def main():
    parser = argparse.ArgumentParser(description='Different OpenCV similarity computation methods & jpeg')
    parser.add_argument('--filename', dest='filename', required=True)
    parser.add_argument('--output', dest='output_filename', required=True)
    args = parser.parse_args()

    source_image = cv.imread(cv.samples.findFile(args.filename))
    source_image = cv.resize(source_image, (1280, 720))

    compression_from = 0.01
    compression_to = 1.00
    FRAMES = 100 * 3

    out_video = np.empty([FRAMES, source_image.shape[0], source_image.shape[1], 3], dtype = np.uint8)
    out_video = out_video.astype(np.uint8)

    for frame in tqdm(range(FRAMES)):
        cur_compression = compression_from + frame * (compression_to - compression_from) / (FRAMES - 1)
        rs = compress_jpeg_q(source_image, cur_compression)
        mse = compute_mse(rs, source_image)
        psnr = compute_psnr(rs, source_image)
        ssim = compute_ssim(rs, source_image)

        cv.putText(rs, 'compression ' + "{:.3f}".format(cur_compression), (10, 30), cv.FONT_HERSHEY_PLAIN, 1, (255, 255, 255), 2, cv.LINE_4)
        cv.putText(rs, 'MSE  ' + "{:.3f}".format(mse), (10, 50), cv.FONT_HERSHEY_PLAIN, 1, (255, 255, 255), 2, cv.LINE_4)
        cv.putText(rs, 'PSNR ' + "{:.3f}".format(psnr), (10, 70), cv.FONT_HERSHEY_PLAIN, 1, (255, 255, 255), 2, cv.LINE_4)
        cv.putText(rs, 'SSIM ' + "{:.3f}".format(ssim), (10, 90), cv.FONT_HERSHEY_PLAIN, 1, (255, 255, 255), 2, cv.LINE_4)

        out_video[frame] = cv.merge([rs[:,:,2], rs[:,:,1], rs[:,:,0]])

    skvideo.io.vwrite(args.output_filename, out_video, outputdict={"-pix_fmt": "yuv420p"})

if __name__ == "__main__":
    main()