# Wiener filter
## Как запустить
TL;DR
```sh
git clone <repo>
cd <repo>
python -m virtualenv env
source env/bin/activate
pip install -r requirements.txt
```

## Пример работы
```sh
python main.py --filename flower.jpeg --output video.mp4
```

Жмем картинку с разным качеством и считаем метрики

### Исходное изображение
![Исходное изображение](flower.jpeg)

### Полученное видео
![Видео](video.mp4)