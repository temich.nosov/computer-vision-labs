# Шаблон проекта для OpenCV на Python
## Как запустить
TL;DR
```sh
git clone <repo>
cd <repo>
python -m virtualenv env
source env/bin/activate
pip install -r requirements.txt
```


Для работы с проектом используется [venv](https://docs.python.org/3/tutorial/venv.html). Список зависимостей есть в requirements.txt

## Пример работы
```
python main.py --filename flower.jpeg --pixels 120 --output flower_res.jpeg
```

### Исходное изображение
![Исходное изображение](flower.jpeg)

### Результат
![Результат](flower_res.jpeg)