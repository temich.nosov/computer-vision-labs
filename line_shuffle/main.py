import argparse
import cv2 as cv
import numpy as np
import sys

def shuffle_image(filename, pixels, shuffle_rows, save_to=None):
    img = cv.imread(cv.samples.findFile(filename))
    if img is None:
        sys.exit("Could not read the image.")

    if not shuffle_rows:
        img = np.transpose(img, (1, 0, 2))

    width, height, channels = img.shape
    if width % pixels != 0:
        sys.exit("Can not reshape image. {} division by {}".format(width, pixels))
    images = np.reshape(img, (width // pixels, pixels, height, channels))
    np.random.shuffle(images)
    img = np.reshape(images, (width, height, channels))

    if not shuffle_rows:
        img = np.transpose(img, (1, 0, 2))

    if save_to == None:
        cv.imshow("Display window", img)
        k = cv.waitKey(0)
    else:
        cv.imwrite(save_to, img)

def main():
    parser = argparse.ArgumentParser(description='Shuffle lines in image')
    parser.add_argument('--pixels', dest='pixels', default=120, type=int,
        help='Width or height of shuffled line (default: 120px)')
    parser.add_argument('--by-row', dest='row_or_col', action='store_const', const='by_row',
        default='by_col', help='Shuffle rows (default: shuffle columns)')
    parser.add_argument('--filename', dest='filename', required=True)
    parser.add_argument('--output', dest='output_filename')

    args = parser.parse_args()
    shuffle_image(args.filename, args.pixels, args.row_or_col == 'by_row', args.output_filename)

if __name__ == "__main__":
    main()